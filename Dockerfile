FROM tomcat:latest

LABEL maintainer="raj"

ADD ./target/LoginWebApp-1.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"]
